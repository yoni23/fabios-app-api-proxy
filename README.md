# Fabios app API proxy

NGINX proxy app for fabios app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `api`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)

